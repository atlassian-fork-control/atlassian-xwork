package com.atlassian.xwork12.interceptors;

import com.atlassian.xwork12.Xwork12VersionSupport;

/**
 *
 */
public class XsrfTokenInterceptor extends com.atlassian.xwork.interceptors.XsrfTokenInterceptor {
    public XsrfTokenInterceptor() {
        super(new Xwork12VersionSupport());
    }
}
