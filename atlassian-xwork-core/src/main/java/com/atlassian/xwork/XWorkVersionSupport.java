package com.atlassian.xwork;

import com.opensymphony.xwork.Action;
import com.opensymphony.xwork.ActionInvocation;

import java.lang.reflect.Method;

/**
 * Utility interface for abstracting binary or API-incompatible aspects of different XWork versions.
 */
public interface XWorkVersionSupport {
    /**
     * Works around binary incompatibility of ActionInvocation#getAction between XWork 1.0 and 1.2.
     *
     * @param invocation the action invocation
     * @return the associated action
     */
    Action extractAction(ActionInvocation invocation);

    /**
     * Works around missing ActionConfig#getMethod in Xwork 1.2.
     *
     * @param invocation the action invocation
     * @return the associated action
     * @throws NoSuchMethodException if a method could not be found for the action
     */
    Method extractMethod(ActionInvocation invocation) throws NoSuchMethodException;
}
