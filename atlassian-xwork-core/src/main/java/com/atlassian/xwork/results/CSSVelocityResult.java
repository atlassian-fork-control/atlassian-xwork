package com.atlassian.xwork.results;

import com.opensymphony.webwork.dispatcher.VelocityResult;

public class CSSVelocityResult extends VelocityResult {
    protected String getContentType(String s) {
        return "text/css";
    }
}
