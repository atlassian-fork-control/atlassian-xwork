package com.atlassian.xwork.results;

import com.atlassian.util.profiling.UtilTimerStack;
import com.opensymphony.webwork.dispatcher.VelocityResult;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.util.OgnlValueStack;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;

/**
 * A subclass of VelocityResult which adds profiling to execution and template retrieval.
 */
public class ProfiledVelocityResult extends VelocityResult {
    public void doExecute(String finalLocation, ActionInvocation invocation) throws Exception {
        UtilTimerStack.push("XW View: doExecute(" + finalLocation + ")");

        try {
            super.doExecute(finalLocation, invocation);
        } finally {
            UtilTimerStack.pop("XW View: doExecute(" + finalLocation + ")");
        }
    }

    protected Template getTemplate(OgnlValueStack stack, VelocityEngine velocity, ActionInvocation invocation, String location) throws Exception {
        UtilTimerStack.push("XW View: getTemplate(" + location + ")");
        try {
            return super.getTemplate(stack, velocity, invocation, location);
        } finally {
            UtilTimerStack.pop("XW View: getTemplate(" + location + ")");
        }
    }
}
