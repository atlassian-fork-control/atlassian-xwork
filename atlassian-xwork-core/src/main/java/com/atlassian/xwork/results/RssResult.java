package com.atlassian.xwork.results;

import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.dispatcher.WebWorkResultSupport;
import com.opensymphony.xwork.ActionInvocation;
import com.sun.syndication.feed.WireFeed;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.WireFeedOutput;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RssResult extends WebWorkResultSupport {
    private static final Logger log = Logger.getLogger(RssResult.class);
    private static final String DEFAULT_DEFAULT_ENCODING = "UTF-8";

    public static final String RSS = "rss";
    public static final String RSS1 = "rss1";
    public static final String RSS2 = "rss2";
    public static final String ATOM = "atom";

    protected void doExecute(String finalDestination, ActionInvocation actionInvocation) throws Exception {
        // No need to create a session for RSS
        ServletActionContext.getRequest().getSession(false);
        HttpServletResponse response = ServletActionContext.getResponse();

        if (finalDestination.startsWith("rss"))
            response.setContentType("application/rss+xml; charset=" + DEFAULT_DEFAULT_ENCODING);
        else if (finalDestination.startsWith("atom"))
            response.setContentType("application/atom+xml; charset=" + DEFAULT_DEFAULT_ENCODING);
        else
            response.setContentType("text/xml; charset=" + DEFAULT_DEFAULT_ENCODING);

        SyndFeed feed = (SyndFeed) actionInvocation.getStack().findValue("syndFeed");
        if (feed == null)
            throw new ServletException("Unable to find feed for this action");

        WireFeed outFeed = feed.createWireFeed(finalDestination);
        outFeed.setEncoding(DEFAULT_DEFAULT_ENCODING);
        new WireFeedOutput().output(outFeed, response.getWriter());
        try {
            response.flushBuffer();
        } catch (IOException e) {
            log.info("Client aborted (closed the connection) before the rss feed could be returned.");
            if (log.isDebugEnabled()) {
                log.debug("Error sending rss result to client", e);
            }
        }
    }
}