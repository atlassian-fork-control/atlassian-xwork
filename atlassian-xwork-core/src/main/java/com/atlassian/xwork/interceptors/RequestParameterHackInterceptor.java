package com.atlassian.xwork.interceptors;

import com.opensymphony.xwork.ActionContext;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.interceptor.AroundInterceptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This interceptor looks for a map in the ActionContext context, and if it finds it BUT
 * those parameters are NOT in the existing Http Request, then it calls them as setters on the action.
 *
 * @deprecated since 1.20 - To be removed soon, never rely on a hack thing
 */
@SuppressWarnings("unused")
@Deprecated
public class RequestParameterHackInterceptor extends AroundInterceptor {
    private static ThreadLocal hack = new ThreadLocal();

    public static void setHackMap(Map hackMap) {
        com.atlassian.xwork.interceptors.RequestParameterHackInterceptor.hack.set(hackMap);
    }

    protected void before(ActionInvocation invocation) throws Exception {
        Map hackMap = (Map) com.atlassian.xwork.interceptors.RequestParameterHackInterceptor.hack.get();

        if (hackMap != null) {
            Map acParameters = ActionContext.getContext().getParameters();

            List parametersToSetOnAction = new ArrayList(hackMap.size());
            for (Iterator iterator = hackMap.keySet().iterator(); iterator.hasNext(); ) {
                String key = (String) iterator.next();
                if (!acParameters.containsKey(key)) {
                    parametersToSetOnAction.add(key);
                }
            }

            // if we have hack parameters, let's replace the webwork parameters map completely
            if (parametersToSetOnAction.size() > 0) {
                Map parameters = new HashMap(acParameters);
                for (Iterator iterator = parametersToSetOnAction.iterator(); iterator.hasNext(); ) {
                    String key = (String) iterator.next();
                    parameters.put(key, hackMap.get(key));
                }

                ActionContext.getContext().setParameters(parameters);
            }

            com.atlassian.xwork.interceptors.RequestParameterHackInterceptor.setHackMap(null);
        }
    }

    protected void after(ActionInvocation actionInvocation, String s) throws Exception {
    }
}
