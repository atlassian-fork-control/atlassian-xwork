package com.atlassian.xwork.interceptors;

import com.atlassian.util.profiling.ProfilingUtils;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.ActionProxy;
import com.opensymphony.xwork.interceptor.PreResultListener;
import org.apache.log4j.Logger;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.springframework.transaction.interceptor.TransactionAttribute;

/**
 * Invoke an XWork ActionInvocation within a transaction. If the invocation throws an unchecked exception, roll
 * back the txn.
 */
class TransactionalInvocation {
    private static final Logger log = Logger.getLogger(TransactionalInvocation.class);

    private final TransactionAttribute transactionAttribute = new DefaultTransactionAttribute(TransactionAttribute.PROPAGATION_REQUIRED);
    private final PlatformTransactionManager transactionManager;

    private TransactionStatus transactionStatus;

    public TransactionalInvocation(PlatformTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    public String invokeInTransaction(final ActionInvocation invocation) throws Exception {
        if (log.isDebugEnabled())
            log.debug("Creating transaction for action invocation: " + getDetails(invocation));

        setTransactionStatus(getNewTransaction());

        // Add listener to commit transaction between action and result. If we do not do this, we run the risk
        // that if the result is a redirect, the user will follow the redirect before the transaction has been
        // committed, and see stale data.
        invocation.addPreResultListener(new PreResultListener() {
            public void beforeResult(ActionInvocation actionInvocation, String s) {
                commitOrRollbackTransaction(invocation, false);

                if (log.isDebugEnabled())
                    log.debug("Creating transaction for action result: " + getDetails(invocation));

                setTransactionStatus(getNewTransaction());
            }
        });

        boolean swallowCommitErrors = true;
        try {
            String result = invokeAndHandleExceptions(invocation);
            swallowCommitErrors = false;
            return result;
        } finally {
            commitOrRollbackTransaction(invocation, swallowCommitErrors);
        }
    }

    private String invokeAndHandleExceptions(ActionInvocation invocation) throws Exception {
        try {
            return invocation.invoke();
        } catch (Exception ex) {
            handleInvocationException(invocation, transactionAttribute, transactionStatus, ex);
            throw ex;
        }
    }

    private void commitOrRollbackTransaction(ActionInvocation actionInvocation, boolean swallowCommitErrors) {
        try {
            // If you try to commit a transaction that is completed or marked for rollback,
            // you'll get an UnexpectedRollbackException
            if (transactionStatus.isCompleted()) {
                log.error("Action " + getDetails(actionInvocation) + " is already completed and can not be committed again.");
            } else if (transactionStatus.isRollbackOnly()) {
                if (log.isDebugEnabled())
                    log.debug("Transaction status for action " + getDetails(actionInvocation) + " set to rollback only. Invoking rollback()");

                transactionManager.rollback(transactionStatus);
            } else {
                if (log.isDebugEnabled())
                    log.debug("Committing transaction for action " + getDetails(actionInvocation));

                transactionManager.commit(transactionStatus);
            }
        } catch (RuntimeException e) {
            if (swallowCommitErrors) {
                log.error("Commit/Rollback exception occurred but was swallowed", e);
            } else {
                throw e;
            }
        }
    }

    private void handleInvocationException(ActionInvocation invocation, TransactionAttribute txAtt, TransactionStatus status, Throwable ex) {
        if (status == null)
            return;

        if (txAtt.rollbackOn(ex)) {
            log.info("Invoking rollback for transaction on action '" + getDetails(invocation) + "' due to throwable: " + ex, ex);
            status.setRollbackOnly();
        } else if (log.isDebugEnabled()) {
            log.debug("Action " + getDetails(invocation) + " threw exception " + ex + " but did not trigger a rollback.");
        }
    }

    private TransactionStatus getNewTransaction() {
        return transactionManager.getTransaction(transactionAttribute);
    }

    private void setTransactionStatus(TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    private String getDetails(ActionInvocation invocation) {
        ActionProxy proxy = invocation.getProxy();
        String methodName = proxy.getConfig().getMethodName();

        if (methodName == null)
            methodName = "execute";

        String actionClazz = ProfilingUtils.getJustClassName(proxy.getConfig().getClassName());

        return proxy.getNamespace() + "/" + proxy.getActionName() + ".action (" + actionClazz + "." + methodName + "())";
    }

}
