package com.atlassian.xwork.interceptors;

import com.opensymphony.xwork.Action;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.ActionSupport;
import com.opensymphony.xwork.Validateable;
import com.opensymphony.xwork.ValidationAware;
import junit.framework.AssertionFailedError;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class TestSimpleValidationInterceptor extends MockObjectTestCase {
    public void testReturnsInputIfNoErrors() throws Exception {
        Mock mockActionInvocation = mock(ActionInvocation.class);

        Action action = getStubValidatingAction("result", true);
        mockActionInvocation.expects(once()).method("getAction").will(returnValue(action));

        SimpleValidationInterceptor interceptor = new SimpleValidationInterceptor();
        assertEquals("input", interceptor.intercept((ActionInvocation) mockActionInvocation.proxy()));
    }

    public void testInvokesIfNoErrors() throws Exception {
        Mock mockActionInvocation = mock(ActionInvocation.class);

        Action action = getStubValidatingAction("result", false);
        mockActionInvocation.expects(once()).method("getAction").will(returnValue(action));
        mockActionInvocation.expects(once()).method("invoke").will(returnValue(action.execute()));

        SimpleValidationInterceptor interceptor = new SimpleValidationInterceptor();
        assertEquals("result", interceptor.intercept((ActionInvocation) mockActionInvocation.proxy()));
    }

    public void testNonValidatingAction() throws Exception {
        Mock mockActionInvocation = mock(ActionInvocation.class);

        Action action = getStubNonValidatingAction(ActionSupport.SUCCESS);
        mockActionInvocation.expects(once()).method("getAction").will(returnValue(action));
        mockActionInvocation.expects(once()).method("invoke").will(returnValue(action.execute()));

        SimpleValidationInterceptor interceptor = new SimpleValidationInterceptor();
        assertEquals(ActionSupport.SUCCESS, interceptor.intercept((ActionInvocation) mockActionInvocation.proxy()));
    }

    private Action getStubNonValidatingAction(final String result) {
        return (Action) Proxy.newProxyInstance(getClass().getClassLoader(),
                new Class[]{Action.class},
                new InvocationHandler() {
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        if (method.getName().equals("execute"))
                            return result;
                        if (method.getName().equals("toString"))
                            return "ProxyAction: [ result: " + result + "]";
                        throw new AssertionFailedError("Unexpected method call: " + method.getName());
                    }
                });
    }

    private Action getStubValidatingAction(final String result, final boolean hasErrors) {
        return (Action) Proxy.newProxyInstance(getClass().getClassLoader(),
                new Class[]{Action.class, Validateable.class, ValidationAware.class},
                new InvocationHandler() {
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        if (method.getName().equals("execute"))
                            return result;
                        if (method.getName().equals("hasErrors"))
                            return Boolean.valueOf(hasErrors);
                        if (method.getName().equals("validate"))
                            return null;
                        if (method.getName().equals("toString"))
                            return "ProxyAction: [ result: " + result + ", hasErrors: " + hasErrors + "]";
                        throw new AssertionFailedError("Unexpected method call: " + method.getName());
                    }
                });
    }

}

